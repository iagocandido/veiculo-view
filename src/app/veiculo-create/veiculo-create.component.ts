import { Component, OnInit } from '@angular/core';
import { VeiculoServiceService } from '../veiculo-service.service';

@Component({
  selector: 'app-veiculo-create',
  templateUrl: './veiculo-create.component.html',
  styleUrls: ['./veiculo-create.component.css']
})
export class VeiculoCreateComponent implements OnInit {

  veiculo= {id:'', marca:'', modelo:'', placa:''};

  constructor(private veiculoService: VeiculoServiceService) { }

  ngOnInit(): void {
  }

  salvar(){
    this.veiculoService.post(this.veiculo).subscribe(r => {this.veiculo = {id:'', marca:'', modelo:'', placa:''}})
  }
}

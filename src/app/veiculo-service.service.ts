import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VeiculoServiceService {

  constructor(private http: HttpClient) { }
 
  public get(): Observable<any>{
    return this.http.get('http://localhost:8080/veiculo');
  }
  public post(veiculo): Observable<any>{
    return this.http.post('http://localhost:8080/veiculo', veiculo);
  }
  public delete(id): Observable<any>{
    return this.http.delete(`http://localhost:8080/veiculo/${id}`);
  }
}
